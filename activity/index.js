


/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function printWelcomeName(){
	let fullName = prompt("Enter your Full Name");
	let age = prompt("Enter your age");
	let location = prompt("Enter your location");

	console.log("hello" + fullName +"!");
	console.log("You are" + age);
	console.log("You live in" + location);


}

printWelcomeName();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:


function showBands(){

	let band1 = "Ariana Grande";
	let band2 = "Siakol";
	let band3 = "Chicosi";
	let band4 = "Ben & Ben";
	let band5 = "GIGI De Lana";

	console.log ("1." + band1);
	console.log ("2." + band2);
	console.log ("3." + band3);
	console.log ("4." + band4);
	console.log ("5." + band5);
}

showBands();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:


function movieShows(){

	let mshows1 = "Ironman 1";
	let mshows2 = "Thor Love of thunder";
	let mshows3 = "Avengers Infinity War";
	let mshows4 = "Avengers Age of Ultron";
	let mshows5 = "Avengers End Game";

	let rotTomatoes1 = "Rotten Tomatoes Rating: 94%"
	let rotTomatoes2 = "Rotten Tomatoes Rating: 63%"
	let rotTomatoes3 = "Rotten Tomatoes Rating: 85%"
	let rotTomatoes4 = "Rotten Tomatoes Rating: 76%"
	let rotTomatoes5 = "Rotten Tomatoes Rating: 94%"

	console.log ("1. " + mshows1);
	console.log (rotTomatoes1);
	console.log ("2. " + mshows2);
	console.log (rotTomatoes2);
	console.log ("3. " + mshows3);
	console.log (rotTomatoes3);
	console.log ("4. " + mshows4);
	console.log (rotTomatoes4);
	console.log ("5. " + mshows5);
	console.log (rotTomatoes5);
}

movieShows();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

 // printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

// console.log(friend1);
// console.log(friend2);

printFriends();